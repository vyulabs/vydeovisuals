//
//  VVAppDelegate.h
//  VydeoVisuals
//
//  Created by adarovsky on 03/01/2017.
//  Copyright (c) 2017 adarovsky. All rights reserved.
//

@import UIKit;

@interface VVAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
