//
//  main.m
//  VydeoVisuals
//
//  Created by adarovsky on 03/01/2017.
//  Copyright (c) 2017 adarovsky. All rights reserved.
//

@import UIKit;
#import "VVAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VVAppDelegate class]));
    }
}
