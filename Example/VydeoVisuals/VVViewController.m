//
//  VVViewController.m
//  VydeoVisuals
//
//  Created by adarovsky on 03/01/2017.
//  Copyright (c) 2017 adarovsky. All rights reserved.
//
@import VydeoVisuals;

#import "VVViewController.h"

@interface VVViewController ()

@end

@implementation VVViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showAlert:(id)sender
{
    VVFancyAlertViewController * alert = [[VVFancyAlertViewController alloc] initWithTitle:@"Title" message:@"Message" buttonOrientation:UILayoutConstraintAxisHorizontal];

    [alert addButtonWithImage:nil title:@"Button 1" block:^{
        NSLog(@"button 1 tapped");
    }];

    [alert addButtonWithImage:nil empty:YES title:@"Button 2" block:^{
        NSLog(@"button 2 tapped");
    }];

    [self presentViewController:alert animated:YES completion:nil];
}

@end
