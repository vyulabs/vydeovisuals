# VydeoVisuals

[![CI Status](http://img.shields.io/travis/adarovsky/VydeoVisuals.svg?style=flat)](https://travis-ci.org/adarovsky/VydeoVisuals)
[![Version](https://img.shields.io/cocoapods/v/VydeoVisuals.svg?style=flat)](http://cocoapods.org/pods/VydeoVisuals)
[![License](https://img.shields.io/cocoapods/l/VydeoVisuals.svg?style=flat)](http://cocoapods.org/pods/VydeoVisuals)
[![Platform](https://img.shields.io/cocoapods/p/VydeoVisuals.svg?style=flat)](http://cocoapods.org/pods/VydeoVisuals)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

VydeoVisuals is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "VydeoVisuals"
```

## Author

adarovsky, adarovsky@vyulabs.com

## License

VydeoVisuals is available under the MIT license. See the LICENSE file for more info.
