//
//  CameraPermissionViewController.h
//  FanDate
//
//  Created by Alexander Darovsky on 05.02.16.
//  Copyright © 2016 Vyu Labs, Inc. All rights reserved.
//

@import UIKit;

@interface VVFancyAlertViewController : UIViewController

- (instancetype) initWithImage:(UIImage*)image
                         title:(NSString*)title
                       message:(NSString*)message
             buttonOrientation:(UILayoutConstraintAxis)orientation;


- (instancetype) initWithTitle:(NSString*)title
                       message:(NSString*)message
             buttonOrientation:(UILayoutConstraintAxis)orientation;

- (UIButton*) addButtonWithImage:(UIImage*)image
                           empty:(BOOL)empty
                           title:(NSString*)title
                           block:(void (^)())block;
- (UIButton*) addButtonWithImage:(UIImage*)image
                           title:(NSString*)title
                           block:(void (^)())block;
@end
