//
//  VVTintedImageView.h
//  Pods
//
//  Created by Alexander Darovsky on 05.04.17.
//
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface VVTintedImageView : UIImageView
@end
