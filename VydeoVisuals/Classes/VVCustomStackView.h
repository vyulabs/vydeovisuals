//
//  CustomStackView.h
//  Avatar
//
//  Created by Alexander Darovsky on 26.07.16.
//  Copyright © 2016 Vyu Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface VVCustomStackView : UIView
@property (nonatomic, assign) IBInspectable CGFloat rowHeight;
@property (nonatomic, assign) IBInspectable CGFloat span;
@end
