//
//  UIImage+RoundRect.m
//  Pods
//
//  Created by Alexander Darovsky on 03.05.17.
//
//

#import "UIImage+RoundRect.h"

@implementation UIImage(VydeoUtility)
- (UIImage*) roundImage
{
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 0.0f);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGPathRef path = CGPathCreateWithEllipseInRect(CGRectMake(0, 0, self.size.width, self.size.height), NULL);
    CGContextAddPath(ctx, path);
    CGContextClip(ctx);
    CFRelease(path);
    [self drawAtPoint:CGPointZero];
    UIImage * clipped = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return clipped;
}

- (UIImage*) roundImageWithBorder:(CGFloat)width ofColor:(UIColor*)color
{
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 0.0f);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGPathRef path = CGPathCreateWithEllipseInRect(CGRectMake(0, 0, self.size.width, self.size.height), NULL);
    CGContextAddPath(ctx, path);
    CGContextClip(ctx);
    [self drawAtPoint:CGPointZero];

    CGContextAddPath(ctx, path);
    CGContextSetLineWidth(ctx, width);
    [color setStroke];
    CGContextStrokePath(ctx);

    CFRelease(path);

    UIImage * clipped = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return clipped;
}

+ (UIImage*) circleWithDiameter:(CGFloat)diameter tintedWithColor:(UIColor*)color
{
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(diameter, diameter), NO, 0.0f);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [color set];
    CGContextFillEllipseInRect(ctx, CGRectMake(0, 0, diameter, diameter));
    UIImage * result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return result;
}

+ (UIImage*) roundRectWithCorner:(CGFloat)radius tintedWithColor:(UIColor *)color
{
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(radius*2+1, radius*2+1), NO, 0.0f);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [color set];
    CGPathRef path = CGPathCreateWithRoundedRect(CGRectMake(0, 0, radius*2+1, radius*2+1), radius, radius, NULL);
    CGContextAddPath(ctx, path);
    CGContextFillPath(ctx);
    CFRelease(path);
    UIImage * result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return [result resizableImageWithCapInsets:UIEdgeInsetsMake(radius, radius, radius, radius)];
}

+ (UIImage*) roundRectWithCorner:(CGFloat)radius
                 filledWithColor:(UIColor *)fillColor
                strokedWithColor:(UIColor *)strokeColor
                       lineWidth:(CGFloat)lineWidth
{
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(radius*2+1+lineWidth*2, radius*2+1+lineWidth*2), NO, 0.0f);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGPathRef path = CGPathCreateWithRoundedRect(CGRectMake(lineWidth/2, lineWidth/2, radius*2+lineWidth+1, radius*2+lineWidth+1), radius, radius, NULL);
    CGContextAddPath(ctx, path);
    [fillColor setFill];
    [strokeColor setStroke];
    CGContextSetLineWidth(ctx, lineWidth);
    CGContextDrawPath(ctx, kCGPathFillStroke);
    CFRelease(path);
    UIImage * result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return [result resizableImageWithCapInsets:UIEdgeInsetsMake(radius+lineWidth, radius+lineWidth, radius+lineWidth, radius+lineWidth)];
}

- (UIImage*) imageTintedWithColor:(UIColor *)tintColor
{
    UIImage * that = [self imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImageView * iv = [[UIImageView alloc] initWithImage:that];
    iv.tintColor = tintColor;
    UIGraphicsBeginImageContextWithOptions(iv.frame.size, NO, 0.0);
    [iv drawRect:iv.frame];
    UIImage * newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (UIImage*) imageBlendedWithAlpha:(CGFloat)alpha
{
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 0.0f);

    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGRect area = CGRectMake(0, 0, self.size.width, self.size.height);

    CGContextScaleCTM(ctx, 1, -1);
    CGContextTranslateCTM(ctx, 0, -area.size.height);

    CGContextSetBlendMode(ctx, kCGBlendModeMultiply);

    CGContextSetAlpha(ctx, alpha);

    CGContextDrawImage(ctx, area, self.CGImage);

    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();

    return newImage;
}

@end
