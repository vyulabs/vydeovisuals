//
//  CustomStackView.m
//  Avatar
//
//  Created by Alexander Darovsky on 26.07.16.
//  Copyright © 2016 Vyu Labs, Inc. All rights reserved.
//
#import "VVCustomStackView.h"

@interface NSArray<ObjectType> (VVCustomStackTraversal)
- (void)iterateEachTwo:(void(^)(ObjectType this, ObjectType next))block;
@end

@implementation VVCustomStackView {
    UIView * centerView;
    BOOL deleting;
    BOOL updatingConstraints;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        centerView = [[UIView alloc] initWithFrame:CGRectZero];
        centerView.translatesAutoresizingMaskIntoConstraints = NO;
        deleting = NO;
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        centerView = [[UIView alloc] initWithFrame:CGRectZero];
        centerView.translatesAutoresizingMaskIntoConstraints = NO;
        deleting = NO;
    }
    return self;
}

- (void)dealloc
{
    deleting = YES;
}

- (BOOL) translatesAutoresizingMaskIntoConstraints
{
    return NO;
}

- (CGSize) intrinsicContentSize
{
    NSArray<__kindof UIView*> * sub = [self visibleSubviews];
    NSInteger rows = MAX(1, sub.count-1);
    return CGSizeMake(UIViewNoIntrinsicMetric, rows * self.rowHeight + (rows - 1) * self.span);
}

+ (BOOL) requiresConstraintBasedLayout
{
    return YES;
}

- (void) didAddSubview:(UIView *)subview
{
    [super didAddSubview:subview];
    [subview addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionNew context:NULL];
    if (!deleting && !updatingConstraints)
        dispatch_async(dispatch_get_main_queue(), ^{
            [self invalidateIntrinsicContentSize];
            [self setNeedsUpdateConstraints];
        });
}

- (void) willRemoveSubview:(UIView *)subview
{
    [super willRemoveSubview:subview];

    [subview removeObserver:self forKeyPath:@"hidden"];

    if (!deleting && !updatingConstraints)
        dispatch_async(dispatch_get_main_queue(), ^{
            [self invalidateIntrinsicContentSize];
            [self setNeedsUpdateConstraints];
        });
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    if (!deleting && !updatingConstraints && [keyPath isEqualToString:@"hidden"]) {
        [self invalidateIntrinsicContentSize];
        [self setNeedsUpdateConstraints];
    }
}

- (NSArray<__kindof UIView*> *) visibleSubviews
{
    NSMutableArray<__kindof UIView*> * sub = [NSMutableArray arrayWithCapacity:self.subviews.count];
    for (UIView * v in self.subviews) {
        if (!v.hidden)
            [sub addObject:v];
    }

    return sub;
}

- (void) updateConstraints
{
    updatingConstraints = YES;

    [self removeConstraints:self.constraints];

    [centerView removeFromSuperview];

    for (UIView * v in self.subviews) {
        v.translatesAutoresizingMaskIntoConstraints = NO;
        [v removeConstraints:v.constraints];
    }

    NSArray<__kindof UIView*> * sub = [self visibleSubviews];

    [self removeConstraints:self.constraints];

    for (UIView * v in sub) {
        [v removeConstraints:v.constraints];
        [v addConstraint:[NSLayoutConstraint constraintWithItem:v
                                                      attribute:NSLayoutAttributeHeight
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:nil
                                                      attribute:NSLayoutAttributeNotAnAttribute
                                                     multiplier:1
                                                       constant:self.rowHeight]];
    }

    if (sub.count == 0) {
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1
                                                          constant:0]];
    }
    else if (sub.count == 1) {
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[v]|"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:@{@"v": sub.firstObject}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[v]|"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:@{@"v": sub.firstObject}]];
    }
    else if (sub.count > 1) {
        [self addSubview:centerView];
        [centerView addConstraint:[NSLayoutConstraint constraintWithItem:centerView
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:nil
                                                               attribute:NSLayoutAttributeNotAnAttribute
                                                              multiplier:1
                                                                constant:self.rowHeight]];
        [centerView addConstraint:[NSLayoutConstraint constraintWithItem:centerView
                                                               attribute:NSLayoutAttributeWidth
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:nil
                                                               attribute:NSLayoutAttributeNotAnAttribute
                                                              multiplier:1
                                                                constant:self.span]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:centerView
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1
                                                          constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:centerView
                                                         attribute:NSLayoutAttributeBottom
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeBottom
                                                        multiplier:1
                                                          constant:0]];

        UIView * v1 = sub[0];
        UIView * v2 = sub[1];
        NSArray * __block currentViews = @[v2, centerView, v1];

        [self addConstraint:[NSLayoutConstraint constraintWithItem:v1
                                                         attribute:NSLayoutAttributeTrailing
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeTrailing
                                                        multiplier:1
                                                          constant:0]];

        [currentViews iterateEachTwo:^(UIView * this, UIView * next) {
            [self addConstraint:[NSLayoutConstraint constraintWithItem:this
                                                             attribute:NSLayoutAttributeTrailing
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:next
                                                             attribute:NSLayoutAttributeLeading
                                                            multiplier:1
                                                              constant:0]];
        }];

        [currentViews enumerateObjectsUsingBlock:^(UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self addConstraint:[NSLayoutConstraint constraintWithItem:obj
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self
                                                             attribute:NSLayoutAttributeBottom
                                                            multiplier:1
                                                              constant:0]];
        }];

        [self addConstraint:[NSLayoutConstraint constraintWithItem:v2
                                                         attribute:NSLayoutAttributeLeading
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeLeading
                                                        multiplier:1
                                                          constant:0]];


        [sub enumerateObjectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(2, sub.count - 2)] options:0 usingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[obj]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(obj)]];
            [currentViews enumerateObjectsUsingBlock:^(UIView * _Nonnull v, NSUInteger idx, BOOL * _Nonnull stop) {
                [self addConstraint:[NSLayoutConstraint constraintWithItem:v
                                                                 attribute:NSLayoutAttributeTop
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:obj
                                                                 attribute:NSLayoutAttributeBottom
                                                                multiplier:1
                                                                  constant:self.span]];
            }];
            currentViews = @[obj];
        }];

        [currentViews enumerateObjectsUsingBlock:^(UIView * _Nonnull v, NSUInteger idx, BOOL * _Nonnull stop) {
            [self addConstraint:[NSLayoutConstraint constraintWithItem:v
                                                             attribute:NSLayoutAttributeTop
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1
                                                              constant:0]];
        }];
    }

    [super updateConstraints];

    updatingConstraints = NO;
}
@end

@implementation NSArray (VVCustomStackTraversal)
- (void)iterateEachTwo:(void(^)(id this, id next))block {
    if (self.count < 2) return;

    for (NSUInteger index = 0; index < self.count - 1; index++) {
        id this = [self objectAtIndex:index];
        id next = [self objectAtIndex:index + 1];
        block(this, next);
    }
}

@end
